package com.example.netanel.countriestest;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class SortingArrayTest {

    @Test
    public void orderSize() {
        ArrayList<Country> cArrayList = new ArrayList<>();
        String[]borders = new String[]{"BE","ET"};
        Country country = new Country("blabla","America","500",borders,"Am");
        Country country1 = new Country("blabla","Israel","400",borders,"Am");
        Country country2 = new Country("blabla","Moroo","450",borders,"Am");
        Country country3 = new Country("blabla","Test","352",borders,"Am");
        Country country4 = new Country("blabla","Test1","900",borders,"Am");
        Country country5 = new Country("blabla","Test2","700",borders,"Am");
        Country country6 = new Country("blabla","Test3","500",borders,"Am");
        Country country7 = new Country("blabla","Test4","50",borders,"Am");
        Country country8 = new Country("blabla","Test5","500",borders,"Am");
        cArrayList.add(country);
        cArrayList.add(country1);

        cArrayList.add(country2);
        cArrayList.add(country3);
        cArrayList.add(country4);
        cArrayList.add(country5);
        cArrayList.add(country6);
        cArrayList.add(country7);
        cArrayList.add(country8);
        SortingArray sortingArray = new SortingArray();
        sortingArray.orderSize(cArrayList);
    }
}