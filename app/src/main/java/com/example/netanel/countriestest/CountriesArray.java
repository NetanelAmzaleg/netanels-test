package com.example.netanel.countriestest;

import java.util.ArrayList;

public class CountriesArray {

    private ArrayList<Country> countryArrayList;

    public ArrayList<Country> getCountryArrayList() {
        return countryArrayList;
    }

    public void setCountryArrayList(ArrayList<Country> countryArrayList) {
        this.countryArrayList = countryArrayList;
    }

    private static final CountriesArray ourInstance = new CountriesArray();

    public static CountriesArray getInstance() {
        return ourInstance;
    }

    private CountriesArray() {
    }
}
