package com.example.netanel.countriestest;

import com.google.gson.annotations.SerializedName;

public class Country {
    @SerializedName("nativeName")
    String nativeName;
    @SerializedName("name")
    String enName;
    @SerializedName("area")
    String area;
    @SerializedName("borders")
    String [] borders;
    @SerializedName("alpha3Code")
    String  alpha2Code;

    public Country(String nativeName, String enName, String area, String[] borders, String alpha2Code) {
        this.nativeName = nativeName;
        this.enName = enName;
        this.area = area;
        this.borders = borders;
        this.alpha2Code = alpha2Code;
    }

    public String getNativeName() {
        return nativeName;
    }

    public void setNativeName(String nativeName) {
        this.nativeName = nativeName;
    }

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String[] getBorders() {
        return borders;
    }

    public void setBorders(String[] borders) {
        this.borders = borders;
    }

    public String getAlpha2Code() {
        return alpha2Code;
    }

    public void setAlpha2Code(String alpha2Code) {
        this.alpha2Code = alpha2Code;
    }
}
