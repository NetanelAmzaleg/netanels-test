package com.example.netanel.countriestest;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import java.util.ArrayList;

public class CountryActivity extends AppCompatActivity {

    TextView textViewCountryNameEn,textViewNativeName,textViewArea,textViewAlphToCode,textViewBorders;
    ArrayList<Country> cArrayList ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);
        textViewCountryNameEn= findViewById(R.id.textViewCountryNameEn);
        textViewNativeName= findViewById(R.id.textViewNativeName);
        textViewArea= findViewById(R.id.textViewArea);
        textViewAlphToCode= findViewById(R.id.textViewAlphToCode);
        textViewBorders = findViewById(R.id.textViewBorders);
        int location = getIntent().getIntExtra("Location",0);

        cArrayList =CountriesArray.getInstance().getCountryArrayList();

        textViewCountryNameEn.setText(cArrayList.get(location).getEnName());
        textViewNativeName.setText(cArrayList.get(location).getNativeName());
        textViewArea.setText(cArrayList.get(location).getArea());
        textViewAlphToCode.setText(cArrayList.get(location).getAlpha2Code());
        String[] borders = cArrayList.get(location).getBorders();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i <borders.length-1;i++){
            for (int j = 0; j<cArrayList.size()-1;j++){
                if (borders[i].equals(cArrayList.get(j).getAlpha2Code())){
                    stringBuilder.append(cArrayList.get(j).getEnName()+", ");
                }
            }

        }
        try {
            stringBuilder.deleteCharAt(stringBuilder.lastIndexOf(","));
            stringBuilder.deleteCharAt(stringBuilder.lastIndexOf(" "));
            stringBuilder.append(".");
        }catch (StringIndexOutOfBoundsException e){}

        textViewBorders.setText(stringBuilder.toString());
    }
}
