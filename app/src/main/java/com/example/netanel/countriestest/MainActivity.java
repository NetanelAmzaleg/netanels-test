package com.example.netanel.countriestest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    Button buttonAlph, buttonSize;
    RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerViewCountry mAdapter;
    private ArrayList<Country> cArrayList  = new ArrayList<>();
    SearchView searchView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonAlph = findViewById(R.id.buttonAlph);
        buttonSize = findViewById(R.id.buttonSize);
        recyclerView = findViewById(R.id.recyclerView);
        searchView = findViewById(R.id.searcView);


        layoutManager = new LinearLayoutManager(MainActivity.this);
        ((LinearLayoutManager) layoutManager).setOrientation(RecyclerView.VERTICAL);
        mAdapter = new RecyclerViewCountry((ArrayList<Country>) cArrayList);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);
        getCountries();


        buttonAlph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SortArray sortArray = new SortArray();
                sortArray.orederAlph();
                mAdapter.updateList(CountriesArray.getInstance().getCountryArrayList());

            }
        });

        buttonSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SortArray sortArray = new SortArray();
                sortArray.orderSize();
                mAdapter.updateList(CountriesArray.getInstance().getCountryArrayList());

            }
        });

        mAdapter.setOnItemClickListener(new RecyclerViewCountry.OnItemClickedListener() {
            @Override
            public void onItemClick(int i) {
                Intent intent =  new Intent(MainActivity.this, CountryActivity.class);
                intent.putExtra("Location",i);
                startActivity(intent);
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) { ArrayList<Country> countryArrayList =new ArrayList<>();
//                        = CountriesArray.getInstance().getCountryArrayList();
                for (int i = 0; i<CountriesArray.getInstance().getCountryArrayList().size()-1; i++){
                    if(CountriesArray.getInstance().getCountryArrayList().get(i).getEnName().contains(s)){
                        countryArrayList.add(CountriesArray.getInstance().getCountryArrayList().get(i));

                    }
                }
                mAdapter.updateList(countryArrayList);
                return false;
            }
        });
    }





    private void getCountries() {


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        Api api = retrofit.create(Api.class);

        Call<List<Country>> call = api.getCountry();


        call.enqueue(new Callback<List<Country>>() {
            @Override
            public void onResponse(Call<List<Country>> call, Response<List<Country>> response) {
                List<Country> countryList = response.body();
                CountriesArray.getInstance().setCountryArrayList((ArrayList<Country>) countryList);
                 mAdapter.updateList((ArrayList<Country>) countryList);
                 buttonAlph.setVisibility(View.VISIBLE);
                 buttonSize.setVisibility(View.VISIBLE);

            }

            @Override
            public void onFailure(Call<List<Country>> call, Throwable t) {

                Toast.makeText(MainActivity.this,"Faild to load"+t.toString(),Toast.LENGTH_LONG).show();
            }
        });
    }
}
