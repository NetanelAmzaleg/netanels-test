package com.example.netanel.countriestest;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class RecyclerViewCountry extends RecyclerView.Adapter<RecyclerViewCountry.RecyclerViewHolder> {
    private ArrayList<Country> countryArrayList;
    private RecyclerViewCountry.OnItemClickedListener mListener;


    public interface OnItemClickedListener {
        void onItemClick(int i);
    }

    public static class RecyclerViewHolder extends RecyclerView.ViewHolder {

        public TextView textViewNative;
        public TextView textViewEnName;
        public LinearLayout layoutCountry;

        public RecyclerViewHolder(@NonNull View itemView, final RecyclerViewCountry.OnItemClickedListener listener) {
            super(itemView);

            this.textViewEnName = itemView.findViewById(R.id.textViewEnName);
            this.textViewNative = itemView.findViewById(R.id.textViewNative);
            this.layoutCountry = itemView.findViewById(R.id.layoutCountry);

            layoutCountry.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (listener != null) {
                        int position = RecyclerViewHolder.this.getAdapterPosition();
                        if (position != -1) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }
    }

    public void setOnItemClickListener(RecyclerViewCountry.OnItemClickedListener listener) {
        this.mListener = listener;
    }

    public RecyclerViewCountry(ArrayList<Country> countryArrayList) {
        this.countryArrayList = countryArrayList;
    }
    public void updateList(ArrayList<Country> countryArrayList){
        this.countryArrayList = countryArrayList;
        notifyDataSetChanged();
    }

    @NonNull
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new RecyclerViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.country_detail, viewGroup, false), this.mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder recyclerViewHolder, int i) {
        Country currentItem = (Country) this.countryArrayList.get(i);
        recyclerViewHolder.textViewNative.setText(currentItem.getNativeName());
        recyclerViewHolder.textViewEnName.setText(currentItem.getEnName());
    }


    public int getItemCount() {
        return this.countryArrayList.size();
    }
}
