package com.example.netanel.countriestest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class SortArray {

    public void orederAlph() {
        ArrayList<Country> cArrayList  = CountriesArray.getInstance().getCountryArrayList();
        Collections.sort(cArrayList, new Comparator<Country>() {
            @Override
            public int compare(Country country, Country t1) {
                return country.getEnName().compareTo(t1.getEnName());


            }
        });
        CountriesArray.getInstance().setCountryArrayList(cArrayList);

    }


    public void orderSize() {

        ArrayList<Country> cArrayList  = CountriesArray.getInstance().getCountryArrayList();
        for (int i=0;i<cArrayList.size()-1;i++){
            for (int j = 0;  j < cArrayList.size()-1;j++) {

                if (cArrayList.get(j).getArea()== null){
                    cArrayList.get(j).setArea("0");
                }
                if ((Double.valueOf(cArrayList.get(i).getArea())) > (Double.valueOf(cArrayList.get(j).getArea()))) {
                    Country country = cArrayList.get(j);
                    cArrayList.set(j, cArrayList.get(i));
                    cArrayList.set(i, country);

                }

            }

        }
        CountriesArray.getInstance().setCountryArrayList(cArrayList);

    }

}
